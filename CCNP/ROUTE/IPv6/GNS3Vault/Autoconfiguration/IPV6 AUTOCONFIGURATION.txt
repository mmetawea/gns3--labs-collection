IPV6 AUTOCONFIGURATION
WRITTEN BY RENE MOLENAAR ON 11 APRIL 2011. POSTED IN IPV6
SCENARIO:
You are working as the network engineer for an Asian based company and responsible for the deployment of IPv6. You heard some good things about this new protocol and are interested in the new autoconfiguration feature, let's see if you can configure this.

GOAL:
Configure the unique local address FD00:12::1/64 on router Red's f0/0 interface.
Configure router Blue to learn its ipv6 address on the f0/0 interface and uses router Red as the default gateway.
IOS:
c3640-jk9s-mz.124-16.bin
